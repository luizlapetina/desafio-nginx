const express = require('express')
const mysql = require("mysql");
const app = express()
const port = 3000
const config = {
    host: 'db',
    user: 'root',
    password: 'root',
    database: 'nodedb'
};

app.get('/', (req, res) => {

    const mysql = require('mysql')
    const connection = mysql.createConnection(config)
    connection.query("drop table if exists people")
    connection.query("create table people(id int not null auto_increment, name varchar(255), primary key(id))")
    connection.query("insert into people(name) values('Luiz Lapetina')")

    connection.query("SELECT * FROM people", function (err, result, fields) {
        if (err) throw err;

        body = '<h1>Full Cycle Rocks!</h1>';
        body += '<h2>'+ result[0].name + '</h2>';

        res.send(body)

    });

    connection.end()
})

// app.get('/', (req, res) => {
//     const mysql = require('mysql')
//     const connection = mysql.createConnection(config)
//     connection.query("drop table if exists people")
//     connection.query("create table people(id int not null auto_increment, name varchar(255), primary key(id))")
//     connection.query("insert into people(name) values('Luiz Lapetina')")
//     connection.query("SELECT * FROM people", function (err, result, fields) {
//         if (err) throw err;
//
//         let body = '<h1>Full Cycle Rocks!</h1>';
//         body += '<h2>'+ result[i].name + '</h2>';
//
//         res.send(body)
//
//     });
//     connection.end()
// })

app.listen(port, () => {
    console.log('rodando na porta ' + port)

})